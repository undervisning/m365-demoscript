# SCRIPT RUNS AFTER 01-FormatTISIPUsers.ps1
Connect-MgGraph -Scopes "User.ReadWrite.All","Group.ReadWrite.All","Directory.ReadWrite.All","RoleManagement.ReadWrite.Directory"

[string] $importfinal = '/m365-final.csv'
[string] $exportsuccess = '/m365-success.csv'
[string] $exportfinalsendmail = '/m365-finalsendmail.csv'
[string] $usersalreadyexistpath = '/m365-exists.csv'
#[string] $usersalreadyexistcleenup = '/m365-exists-cleenup.csv'

#####################################
# -- Do not edit below this part -- #
#####################################

$successUsers = @()
$userexists = @()

$group_licens="o365_licensed_stud" #<- som medlem i denne gruppen blir studenten lisensiert
# o365_licensed_stud er medlem av gruppen o365_pswd_reset som lar studentene kjøre igjennom glemt passord-wizard

# Import av listen som inneholder studentene laget av scriptet 01-FormatTISIPUsers.ps1
$fagcsv=Import-Csv $importfinal -Delimiter ","

foreach ($user in $fagcsv) {
    Write-Host ""
    Write-Host "###############################"
    Write-Host "User info:" -ForegroundColor Green
    Write-Host $user.GivenName -ForegroundColor Green
    Write-Host $user.SurName -ForegroundColor Green
    Write-Host $user.DisplayName -ForegroundColor Green
    Write-Host $user.UserPrincipalName -ForegroundColor Green
    Write-Host $user.AltMail -ForegroundColor Green
    Write-Host $user.Password -ForegroundColor Green
    Write-Host $user.MailNickName -ForegroundColor Green
    Write-Host "###############################"
    Write-Host ""
    $userupn = $User.UserPrincipalName
    $userupn = $userupn.ToString()

    $mgraphuser = Get-MgUser -All | Where-Object {$_.UserPrincipalName -eq "$userupn"}
        if($mgraphuser){ Write-Host "User: $userupn was found - NOT CREATED" -ForegroundColor Yellow
        $userexists += $userupn
        }
            else{ Write-Host "User $userupn was not found - USER WILL BE CREATED" -ForegroundColor Green
            $PasswordProfile = @{
                Password = '**************'
            }  
            New-MgUser `
                -GivenName $user.GivenName `
                -SurName $user.Surname `
                -DisplayName $user.DisplayName `
                -UserPrincipalName $user.UserPrincipalName `
                -MailNickName $user.MailNickName `
                -OtherMails $user.AltMail `
                -PasswordProfile $PasswordProfile `
                -AccountEnabled

                $line = New-Object -TypeName PSObject
                Add-Member -InputObject $line -MemberType NoteProperty -Name GivenName -Value $user.GivenName
                Add-Member -InputObject $line -MemberType NoteProperty -Name SurName -Value $user.Surname
                Add-Member -InputObject $line -MemberType NoteProperty -Name UserPrincipalName -Value $user.UserPrincipalName
                Add-Member -InputObject $line -MemberType NoteProperty -Name DisplayName -Value "$($user.GivenName) $($user.Surname)"
                Add-Member -InputObject $line -MemberType NoteProperty -Name AltMail -Value $user.AltMail

                $successUsers += $line
                
            }
}

$successUsers | Export-Csv -Path $exportsuccess -NoTypeInformation -Encoding 'UTF8'
Import-CSV -Path $exportsuccess | ConvertTo-CSV -NoTypeInformation | % { $_ -Replace '"', ""} | Out-File $exportfinalsendmail -Encoding utf8
$userexists | Export-Csv -Path $usersalreadyexistpath -NoTypeInformation -Encoding 'UTF8'

# Add user to group from m365-finalesendmail.csv
$newusers=Import-Csv $exportfinalsendmail -Delimiter ","
foreach ($user in $newusers) {
    $GroupID = (Get-MgGroup -Filter "DisplayName eq 'o365_licensed_stud'").id
    $userID = (Get-MgUser -UserId $user.UserPrincipalName).id
    New-MgGroupMember -GroupId $GroupID -DirectoryObjectId $userID
    Write-Host "Bruker" $user.Userprincipalname "legges til gruppen o365_licensed_stud" -ForegroundColor Green
}
