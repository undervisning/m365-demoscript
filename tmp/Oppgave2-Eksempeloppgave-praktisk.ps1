<#
.NOTES
Filename:
@SEE data.csv // Eksempelfil for innlesing av brukere fra CSV



.DESCRIPTION

Programmet oppretter brukere ved hjelp av en CSV-fil. Denne koden er hentet fra andrnybe_oppg1_script.ps1.
    All kode som omhandler opprettelse av brukere, er mer eller mindre tatt fra min forrige besvarelse
Deretter opprettes gruppe, teams-rom, sharepoint og en felles mail for HR, IT, Developer og Alle ansatte
    Jeg valgte å opprette teams og sharepoint for alle ansatte, da de også har behov for å kommunisere og dele filer
De opprettede brukerne blir deretter plassert i de ulike avdelingene/gruppene.
Så opprettes nye kanaler/tråder til teams-rommene
Jeg har også valgt å sette noen spam-filter policier og malware policier/rules til ulike grupper
Til slutt opprettes rom og utstyr, samt automatisk godkjennelse av bookinger fra Alle ansatte, 
    med unntak av leiebilen som jeg valgte å sette opp egne regler på

    Kode som har blitt hentet fra eksterne kilder - eller som har blitt brukt som inspirasjon til egen kode,
     er tydelig markert over den aktuelle koden

     Eksempeldata til egenprodusert CSV-fil er generert av ChatGPT for å effektivisere arbeidet

    Jeg har prøvd å bevare engelske navn på variabler der det er brukt kode fra learn.microsoft.com,
     og norske navn på andre variabler som er nødvendig for å oppnå ønsket funksjonalitet. 

    Jeg har i denne besvarelsen hardcodet brukerne i form av importert CSV-fil,
     men oppretter UserPrinsiple name ved hjelp av en global konstant $domene. 
        Denne kan endres til å passe hver enkelt sitt system


Kjente feil og mangler (VIKTIG!):
    - Refererer til samtale på mail: 
        New-Mailbox har ikke fungert, og utallige timer med feilsøking ga ikke resultater. 
        Jeg valgte derfor å inkludere -WhatIf, som gjør at koden ikke kjører, men viser hva som ville ha skjedd
        Jeg har derfor ikke fått testet kode knyttet til opprettelse av rom, og tar det for gitt at det fungerer
        HUSK: Fjern -WhatIf før du evt kjører scriptet
    
    - Det har vært litt kluss med modulene som er brukt, så flere av disse er bortkommentert da de allerede er importert
        Fjern kommentarene foran de aktuelle modulene dersom det er nødvendig når du kjører koden
    
    - Scriptet i sin helhet kjører fra start til slutt (gitt at riktige moduler er imoprtert på DITT system), 
        MED UNNTAK AV opprettelse av rom, da dette ikke har latt seg testes. 

    - Ellers kjører scriptet fra start til slutt

    -MERK! Noen ganger oppstår det noe kluss i maskinereiet dersom man connecter til Exchange
         etter å ha connecta til Teams-modulen. Dette er en kjent feil. Se tråd her for mer info:
         https://github.com/PowerShell/vscode-powershell/issues/4178


Forbedringspotensiale:

    - Kunne ha tatt med enda flere tester, for å så komme med evt feilmelding.
        Jeg har inkludert dette på de stedene der jeg ser det mest vesenltig at er inkludert
    - Brukerne er harcodet i programmet. Jeg har ikke brukt tid på å opprette variabler til
        brukerne som blir importert fra CSV, for mer dynamisk kode, da scriptet kun teller 30% av karakteren

#>
#Endre denne til ditt eget domene
$domene = "@zxdwg.onmicrosoft.com"


<#
Import-Module Microsoft.Graph
Import-Module Microsoft.Graph.Authentication
Import-Module Microsoft.Graph.Users
#>


#All kode som omhandler opprettelse av bruker er gjenbruk av egen kode fra forrige oppgave
# Koble til Microsoft Graph API med nødvendige rettigheter. Kode hentet fra emnets git repo
# https://gitlab.com/undervisning/m365/-/blob/master/02-intro-powershell-m365.ps1?ref_type=heads
Connect-MgGraph -Scopes @("User.ReadWrite.All","Group.ReadWrite.All","Directory.ReadWrite.All", "Organization.Read.All")

# Funksjonen nedenfor (Generate-RandomPassword) er generert med hjelp av AI (ChatGPT), og modifisert til å passe eget script
# Setter funksjonen global for å passe til begge valgmulighetene (1/2)
function Generate-RandomPassword {
    $tegn = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}|;:'\,.<>?/"
    $passord = ""
    $tegnLengde = $tegn.Length
    $Passordlengde = 16

    for ($i = 0; $i -lt $Passordlengde; $i++) {
        $randomIndex = Get-Random -Minimum 0 -Maximum $tegnLengde
        $passord += $tegn[$randomIndex]
    }

    return $passord
}

# Be brukeren om å angi filbanen til CSV-filen
$filbane = Read-Host "Skriv inn filbanen til CSV-filen (for eksempel C:\stien\til\brukerdata.csv)"

# Sjekk om filen eksisterer
if (Test-Path $filbane -PathType Leaf) {
    # Les brukerdata fra CSV-filen
    $brukerdata = Import-Csv -Path $filbane

    # Opprett brukere i løkke
    foreach ($data in $brukerdata) {
        $Fornavn = $data.Fornavn
        $Etternavn = $data.Etternavn
        $Brukernavn = ($Fornavn + $Etternavn)
        $Telefonnummer = $data.Telefonnummer
        $SekundarEpost = $data.SekundarEpost
        $Department = $data.Department
        $JobbTittel = $data.JobbTittel
        $UserPrincipalName = ($Brukernavn + $domene)
        $Mail = $UserPrincipalName
        $randomPassord = Generate-RandomPassword

        $User = New-MgUser -DisplayName ($Fornavn + " " + $Etternavn) `
            -UserPrincipalName $UserPrincipalName `
            -PasswordProfile @{
                ForceChangePasswordNextSignIn = $true
                Password = $randomPassord
            } -AccountEnabled `
            -MailNickname $Brukernavn `
            -OfficeLocation $Department `
            -MobilePhone $Telefonnummer `
            -GivenName $Fornavn `
            -SurName $Etternavn `
            -Mail $Mail `
            -OtherMails @($SekundarEpost) `
            -JobTitle $JobbTittel `
            -Department $Department

        # Sjekk om brukeren skal få lisens (hvis "lisens" er 1 i CSV-filen)
        if ($data.Lisens -eq 1) {
            # Sett UsageLocation til Norway
            $UsageLocation = "NO"
            Update-MgUser -UserId $User.Id -UsageLocation $UsageLocation

            # SKU nummer er hentet fra Microsofts dokumentasjon
            $SkuPartNumber = 'DEVELOPERPACK_E5'
            $EmsSku = Get-MgSubscribedSku -All | Where-Object { $_.SkuPartNumber -eq $SkuPartNumber }
            
            #Hvis Sku er funnet og ingen feil, else; feilmelding
            if ($EmsSku -ne $null) {
                Set-MgUserLicense -UserId $User.Id -AddLicenses @{ SkuId = $EmsSku.SkuId } -RemoveLicenses @()
                Write-Host "Lisensen er lag til for bruker $($User.DisplayName)!"
            } else {
                Write-Host "Lisens med SKU '$SkuPartNumber' finnes ikke"
            }
        } else {
            Write-Host "Brukeren $($User.DisplayName) har ikke fått lisens."
        }
    }
} else {
    Write-Host "Filen eksisterer ikke på den angitte banen."
}




################### Opprette grupper, teams, sharepoint #################




# Sjekk om ExchangeOnlineManagement-modulen er installert, og installer den hvis ikke
if (-not (Get-Module -Name ExchangeOnlineManagement -ListAvailable)) {
    Install-Module -Name ExchangeOnlineManagement -Force -AllowClobber
}

#Import-Module ExchangeOnlineManagement
if (-not (Get-Module -Name MicrosoftTeams -ListAvailable)) {
    Install-Module -Name MicrosoftTeams -Force -AllowClobber
}
# Koble til Microsoft Teams-tjenesten
Connect-MicrosoftTeams

# Oppretter teams/grupper ved hjelp av en funksjon med fire paramtere. Har implementert try catch for feilmelding
function Opprett-TeamsRom {
    param (
        [string]$teamName,
        [string]$teamDescription,
        [string]$teamMailNickname,
        [string]$teamOwner
    )

    try {
        New-Team -DisplayName $teamName -Description $teamDescription -MailNickName $teamMailNickname -Owner $teamOwner -Visibility "public"
        #Avhengig av hvilken version av modulene man benytter, hender det at denne linjen er nødvendig 
        # for at teamsa skal væære synlig i exhchange. Mulig det oppstår feilmld på ditt stystem
        Set-UnifiedGroup -Identity $teamMailNickname -HiddenFromExchangeClientsEnabled:$false
        Write-Host "Gruppe, teams, sharepoint og mail for $teamName opprettet vellykket."
    }
    catch {
        Write-Host "Feil ved opprettelse av $($teamName): $_" -ForegroundColor Red
    }
}

# Opprett Teams-rom for forskjellige avdelinger
Opprett-TeamsRom -teamName "IT" -teamDescription "IT" -teamMailNickname "ITavdeling" -teamOwner "AdamSmith$domene"
Opprett-TeamsRom -teamName "HR" -teamDescription "HR" -teamMailNickname "HRavdeling" -teamOwner "AdamSmith$domene"
Opprett-TeamsRom -teamName "Developer" -teamDescription "Developer" -teamMailNickname "DeveloperAvdeling" -teamOwner "AdamSmith$domene"
Opprett-TeamsRom -teamName "AlleAnsatte" -teamDescription "AlleAnsatte" -teamMailNickname "AlleAnsatte" -teamOwner "AdamSmith$domene"

#Det tar litt tid før gruppene opprettes, og jeg får feilmelding hvis jeg ikke venter noen sekunder
Start-Sleep -Seconds 30



<#
En kompakt kode som legger brukerene til i gruppene sine. Lager først en array med navnet på gruppen, 
og medlemmene i en egen liste/array innad i arrayen.
Løkken øker med i+=2 for å gå videre til neste gruppe, da de to påfølgende elementene tilhører EN gruppe

Inne i løkken henter vi GroupId for den gjeldende gruppen ved å bruke Get-Team med gruppenavnet som ble lagret i $groups[$i].
Dette gir oss riktig groupId for den gjeldende gruppen.

Til slutt hentes brukernavnene for den gjeldende gruppen fra $groups[$i + 1] og lagrer dem i $groupUsers
#>
$groups = @(
    "IT"            , @("AdamSmith$domene", "AlexWilliams$domene"),
    "HR"            , @("AliceJohnson$domene", "AmyDavis$domene"),
    "Developer"     , @("AndrewAnderson$domene", "AvaWhite$domene"),
    "AlleAnsatte"   , @("AdamSmith$domene", "AlexWilliams$domene", "AliceJohnson$domene", "AmyDavis$domene", "AndrewAnderson$domene", "AvaWhite$domene")
)

for ($i = 0; $i -lt $groups.Length; $i += 2) {
    $groupId = (Get-Team -MailNickName $groups[$i]).GroupId
    $groupUsers = $groups[$i + 1]
    foreach ($user in $groupUsers) {
        Add-TeamUser -GroupId $groupId -User $user
    }
}

# Lager nye teamskanalar til team-rommene.
# Kunne selvfølgelig ha gjort dette om til en funksjon også, men ble mer leselig å ta en og en
# Jeg prøvde å sette en kanal til "-MembershipType Private", men det ble noe kluss med eier av gruppen
$gruppe = Get-Team -DisplayName "HR"
$gruppeid = $gruppe.GroupId
New-TeamChannel -GroupId $gruppeid -DisplayName "Nye ansatte"
New-TeamChannel -GroupId $gruppeid -DisplayName "Møteplalegging"

$gruppe = Get-Team -DisplayName "IT"
$gruppeid = $gruppe.GroupId
New-TeamChannel -GroupId $gruppeid -DisplayName "IT-Teknisk"
New-TeamChannel -GroupId $gruppeid -DisplayName "Support"

$gruppe = Get-Team -DisplayName "Developer"
$gruppeid = $gruppe.GroupId
New-TeamChannel -GroupId $gruppeid -DisplayName "Nytt prosjekt"



########### Setter noen forskjellige spam-filter regler og malware policyes/rules ###############



#https://learn.microsoft.com/en-us/powershell/module/exchange/set-mailboxjunkemailconfiguration?view=exchange-ps

Connect-ExchangeOnline
#Vi kan i et tenkt tilfelle si at ingen eposter til HR skal settes til "junk", da de ofte mottar mail utenfor organisasjonen
#NB! Dette er naturligvis ikke lurt, men jeg har her implementert det kun for å vise at det er mulig
Set-MailboxJunkEmailConfiguration -Identity "IT" -Enabled $true
Set-MailboxJunkEmailConfiguration -Identity "HR" -Enabled $false
Set-MailboxJunkEmailConfiguration -Identity "Developer" -Enabled $true

# Vi ønsker at Developer skal kunne samarbeide med NTNU og Sintef, da de har et pågående prosjekt med de.
# Jeg velger derfor truste NTNU- og Sintef-domenet, slik at det slipper gjennom spam-filteret
#
Set-MailboxJunkEmailConfiguration "Developer" -TrustedSendersAndDomains @{Add="ntnu.no","sintef.no"}


<# CyberDyne har også en rekke andre kontakter de trenger å kommunisere med, men som ikke er i organisasjonen vår.
    Vi tillater derfor alle mailboxer å kunne kommunisere med kontaktene vi har lagt inn
#>
Get-MailboxJunkEmailConfiguration "AlleAnsatte" | Where {$_.ContactsTrusted -eq $true} | Set-MailboxJunkEmailConfiguration -ContactsTrusted $false

<# IT-avdelingen har funnet ut at @login.no -domenet brukes til en rekke svindelforsøk,
 og velger derfor å blokkere det for alle ansatte
 #>
Set-MailboxJunkEmailConfiguration "AlleAnsatte"  -BlockedSendersAndDomains @{Add="login.no"} 

<#
Oppretter en ny malware filter policy som blokkerer eposter som inneholder malware i onpremiss Exchange,
 eller blokkerer i Exchange online
En IT-administrator vil bli varslet hvis det blir oppdaget skadevare i en epost fra internt i organisasjonen
#>

#https://learn.microsoft.com/en-us/powershell/module/exchange/new-malwarefilterpolicy?view=exchange-ps
#https://learn.microsoft.com/en-us/powershell/module/exchange/new-malwarefilterrule?view=exchange-ps
New-MalwareFilterPolicy -Name "CyberDyne Malware Filter Policy" -EnableInternalSenderAdminNotifications $true `
 -InternalSenderAdminAddress AdamSmith$domene

 #Dersom mottaker av en mail er i organisasjonen vår, skal malware policien i linjen over applyes
 New-MalwareFilterRule -Name "Alle i CyberDyne" -MalwareFilterPolicy "CyberDyne Malware Filter Policy" -RecipientDomainIs $domene



#################   Lager rom og utstyr    #####################



#https://learn.microsoft.com/en-us/powershell/module/exchange/new-mailbox?view=exchange-ps
#https://gitlab.com/undervisning/m365/-/blob/master/04-configure-exo.ps1?ref_type=heads
# Oppretter en ny mailbox til rommet
# MERK! WhatIf må fjernes før programmet kjøres. Se kommentarer helt øvers i dokumentet for begrunnelse av implementasjon
 New-Mailbox -Name tank$domene `
    -DisplayName "Tank" `
    -Alias "Tank" `
    -Room -EnableRoomMailboxAccount $true `
    -RoomMailboxPassword (ConvertTo-SecureString -String FfdE123e!wes_ -AsPlainText -Force) `
    -WhatIf #FJENR DENNE

# Setter antall plasser i rommet
Get-Mailbox -Identity Tank | Set-Mailbox -ResourceCapacity 12
#https://learn.microsoft.com/en-us/powershell/module/exchange/add-mailboxpermission?view=exchange-ps
#Setter at alle ansatte kan booke romme
Add-MailboxPermission -Identity "Tank" -User "AlleAnsatte" -AccessRights FullAccess
#Rommet skal automatisk godkjenne booking hvis det er ledig i det tidsrommet
Set-CalendarProcessing -Identity "Tank" -AutomateProcessing AutoAccept `
    -DeleteComments $true -AddOrganizerToSubject $true -AllowConflicts $false


# Gjør det samme med Genisys
New-Mailbox -Name genisys$domene `
    -DisplayName "Genisys" `
    -Alias "Genisys" `
    -Room -EnableRoomMailboxAccount $true `
    -RoomMailboxPassword (ConvertTo-SecureString -String FfdE123e!wes_ -AsPlainText -Force) `
    -WhatIf #FJENR DENNE
    
Get-Mailbox -Identity Genisys | Set-Mailbox -ResourceCapacity 8

Add-MailboxPermission -Identity "Genisys" -User "AlleAnsatte" -AccessRights FullAccess
Set-CalendarProcessing -Identity "Genisys" -AutomateProcessing AutoAccept `
    -DeleteComments $true -AddOrganizerToSubject $true -AllowConflicts $false

    #Oppretter en firmabil som ressurs/utstyr som KUN kan bookes av HR

    New-Mailbox -Name firmabil$domene `
    -DisplayName "Firmabil" `
    -Alias "Firmabil" `
    -Equipment `
    -WhatIf #FJENR DENNE

    Add-MailboxPermission -Identity "Firmabil" -User "HR" -AccessRights FullAccess
    # Setter at kun HR kan booke bilen. Da det er så få i avdelingen, vil forespørselen behandles automatisk
    Set-CalendarProcessing -Identity "Firmabil" -AutomateProcessing AutoAccept `
     -BookInPolicy "HRavdeling$domene" -AllBookInPolicy $false
