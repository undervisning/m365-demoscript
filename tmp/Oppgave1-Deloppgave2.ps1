<#
.SYNOPSIS
This script creates Azure AD user accounts based on a CSV file containing user data. 
It also adds users to specified groups, sets up phone authentication methods, and sends SMS notifications.

.PARAMETER CSVFilePath
The path to the CSV file containing user data. The CSV file should have the following format:
DisplayName,MailNickName,UserPrincipalName,Department,JobTitle,Mobile,Country,EmployeeId,Office,Groups

Example: 
Givenname Surname,Givenname.Surname,Givenname.Surname@devhinit.onmicrosoft.com,IT,Manager,90909090,NO,1,Trondheim,"Group1,Group2"

.EXAMPLE
.\Create-AzureADUsers.ps1 -CSVFilePath "NewUsers.csv"

This example runs the script with the specified CSV file to create Azure AD user accounts.
#>

param (
    [Parameter(Mandatory = $true)]
    [string]$CSVFilePath
)


# Connect to Azure AD with the necessary scopes
Connect-MgGraph -Scopes "User.ReadWrite.All", "Group.ReadWrite.All", "UserAuthenticationMethod.ReadWrite.All"

# Prompt for SMTP2GO Credentials for the SMS Gateway 
$SMTPCredentials = Get-Credential -Message "Authentication for SMTP2GO SMS Gateway"

# Mandatory group IDs for users
$mandatoryGroups = @{
    "MFA Group"  = "801f4078-7e33-4149-95f4-e013643d4fa5";
    "SSPR Group" = "fc8a0c17-6ad7-46ac-b7ef-166c0fde266b";
}

# Cache all Azure AD groups
$allGroups = @{}
Get-MgGroup | ForEach-Object {
    $allGroups[$_.DisplayName] = $_.Id
}

# Function to generate a unique password
function GeneratePassword {
    $length = 16
    $uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' | Get-Random
    $lowercase = 'abcdefghijklmnopqrstuvwxyz' | Get-Random
    $number = '0123456789' | Get-Random
    $special = '!@#$&*()-=+?' | Get-Random

    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-=_+[]{}|;:,.<>?`~'
    $basePassword = -join (1..($length - 4) | ForEach-Object { Get-Random -Maximum $characters.length | ForEach-Object { $characters[$_] } })

    $password = -join (($basePassword + $uppercase + $lowercase + $number + $special).ToCharArray() | Get-Random -Count $length)

    return $password
}

# Function to ensure that NO country code is added to a phone number
function EnsureCountryCode {
    param (
        [Parameter(Mandatory = $true)]
        [string]$number
    )
    if (-not $number.StartsWith("+47")) {
        return "+47" + $number
    }
    else {
        return $number
    }
}
# Function to add a phone number as an authentication method for a user
function NewMgUserAuthMethod {
    param (
        [Parameter(Mandatory = $true)]
        [string]$UserPrincipalName,
        
        [Parameter(Mandatory = $true)]
        [string]$phoneNumber
    )

    $retryCount = 0
    $successful = $false
    $maxRetries = 10
    $retryInterval = 5  # interval in seconds

    while ($retryCount -lt $maxRetries -and -not $successful) {
        try {
            New-MgUserAuthenticationPhoneMethod -UserId $UserPrincipalName -phoneType "mobile" -phoneNumber $phoneNumber -ErrorAction Stop
            $successful = $true
        }
        catch {
            if ($_.Exception.Message -like "*The specified user could not be found*") {
                $retryCount++
                Write-Host ("Retrying adding mobile authentication method ($retryCount/$maxRetries) in ${retryInterval}s...") -ForegroundColor Yellow
                Start-Sleep -Seconds $retryInterval 
            }
            else {
                Write-Host ("Failed to add a phone method for {0}. Error: {1}" -f $UserPrincipalName, $_.Exception.Message) -ForegroundColor Red
                break
            }
        }
    }

    return $successful
}

# Import user data from the CSV file
$AADUsers = Import-Csv -Path $CSVFilePath


# Iterate through each user in the CSV and create Azure AD accounts
foreach ($User in $AADUsers) {

    $Password = GeneratePassword

    $PasswordProfile = @{
        Password                             = $Password
        ForceChangePasswordNextSignIn        = $true
        ForceChangePasswordNextSignInWithMfa = $true
    }

    $UserParams = @{
        DisplayName       = $User.DisplayName
        MailNickName      = $User.MailNickName
        UserPrincipalName = $User.UserPrincipalName
        Department        = $User.Department
        JobTitle          = $User.JobTitle
        Mobile            = $User.Mobile
        Country           = $User.Country
        EmployeeId        = $User.EmployeeId
        Office            = $User.Office
        PasswordProfile   = $PasswordProfile
        AccountEnabled    = $true
    }

    try {
        # Create the user based on the specified parameters 
        $newUser = New-MgUser @UserParams -ErrorAction Stop

        # Add to mandatory groups for all users
        foreach ($GroupID in $mandatoryGroups.Values) {
            New-MgGroupMember -GroupId $GroupID -DirectoryObjectId $newUser.Id
        }
        
        # Add the user to groups specified in the CSV file: 
        if ($User.Groups) {
            $groupNames = $User.Groups -split ","
            foreach ($groupName in $groupNames) {
                # Lookup the group from the cache
                $groupID = $allGroups[$groupName]

                if ($groupID) {
                    # Add the user to the specified group 
                    New-MgGroupMember -GroupId $groupID -DirectoryObjectId $newUser.Id
                }
                else {
                    Write-Host ("Group {0} not found." -f $groupName) -ForegroundColor Yellow
                }
            }
        } 

        $phoneNumber = EnsureCountryCode -number $User.Mobile
        
        if (-not (NewMgUserAuthMethod -UserPrincipalName $User.UserPrincipalName -phoneNumber $phoneNumber)) {
            Write-Host ("Failed to add a phone method for {0} after $maxRetries retries." -f $User.DisplayName) -ForegroundColor Red
        }
        
        # Required recipient layout for SMS Gateway
        $smsRecipient = "$phoneNumber@sms.smtp2go.com"
        $smsBody = "Hei, $($User.DisplayName)! Velkommen som ansatt i Contoso. Ditt midlertidige passord er: $Password."
              
        # Send an SMS to the user 
        Send-MailMessage -To $smsRecipient -From 'ikke-svar@test.no' -Body $smsBody -Credential $SMTPCredentials -SmtpServer 'mail.smtp2go.com' -Port 2525
      
        Write-Host ("Successfully created the account for {0}." -f $User.DisplayName) -ForegroundColor Green
    }
    catch {
        Write-Host ("Failed to create the account for {0}. Error: {1}" -f $User.DisplayName, $_.Exception.Message) -ForegroundColor Red
    }
}

# References for the script - used as a basis of inspiration: 
# https://www.sharepointdiary.com/2020/04/powershell-generate-random-password.html
# https://www.alitajran.com/create-azure-ad-users/
# https://support.smtp2go.com/hc/en-gb/articles/6352101968409-SMS-Messaging
# https://mailtrap.io/blog/powershell-send-email/
# https://www.reddit.com/r/PowerShell/comments/w4ktbl/azure_ad_add_group_members_to_multiple_groups/
# https://petri.com/powershell-functions/
